import numpy as np
import matplotlib.pyplot as plt

def gauss(xdata,x0,sigma):
    return (1/(sigma*np.sqrt(2*np.pi)))*np.exp((-(xdata-x0)**2)/(2*sigma**2))

def gauss_window(xdata,x0,sigma):
    return np.exp((-(xdata-x0)**2)/(2*sigma**2))

def read_ibzkpt(path,n_k):
    k_array = np.zeros((n_k, 3))
    mult_array = np.zeros((n_k))
    i=0
    with open(path, 'r') as f:

        for line in f.readlines():

            if i >2:
                if i<3+n_k:
                    k_array[i-3,:] = [float(line.split()[i]) for i in range(3)]
                    mult_array[i-3]= int(line.split()[-1])      


            i+=1

        f.close()
        return k_array, mult_array


orb_index_dict = {"s":0,"py":1,"pz":2,"px":3,"dxy":4,"dyz":5,"dz2":6,"dxz":7,"x2-y2":8,"tot":9}

class dosplot():
    def __init__(self,path, Es,orbs_scf,n_k , n_ion, n_band,sigma_smear,sigma_gauss,ion_sel,orb_sel,nE,show=True,plotlabel="nolabelspec", c = 0):

        self.data_path = path
        self.sigma_smear = sigma_smear
        
        self.ion_sel = ion_sel
        self.orb_sel = orb_sel     

        self.n_k = n_k
        self.n_ion = n_ion
        self.n_bands = n_band
        self.nE = nE
        self.orbs = orbs_scf
        self.Es = Es
        self.c = c
        self.plotlabel=plotlabel
        self.show = show
        
        self.projections = self.get_projections() 
        
        self.k_array,self.mult_array = read_ibzkpt(path+"IBZKPT",self.n_k)
        
        
        gauss_weight = np.repeat(gauss(np.linalg.norm(self.k_array,axis=1),0,sigma_gauss)[np.newaxis,:],self.n_bands,0)

        #proj = np.ones((1,np.shape(gauss_weight)[0],np.shape(gauss_weight)[1]))
        self.weights = gauss_weight*self.projections[0,::,::]
        
        
        self.E_dosplot, self.dosplot, idosplot,dE,norm_factor = self.DOS_full_new()
        
        
        self.plot()


    
    def get_projections(self):
        #Gives the fatbands data for the specified selection of ions and orbitals
        
        projlist = [[self.ion_sel,self.orb_sel]]
        
        proj_array = np.zeros((len(projlist),self.n_bands,self.n_k))
        for i,projection in enumerate(projlist):
            ion_inds = tuple(projection[0])
            # print(ion_inds)
            orb_inds = tuple([orb_index_dict[orb] for orb in projection[1]])
            
            ion_indexed_array = self.orbs[::,::,ion_inds,::]
            orb_indexed_array = ion_indexed_array[::,::,::,orb_inds]
            
            if len(ion_inds)>1:            
                proj_array[i,::,::] = np.sum(orb_indexed_array,axis=(2,3))
                #print(np.sum(orb_indexed_array,axis=(2,3)))
            elif len(orb_inds)>1:            
                proj_array[i,::,::] = np.reshape(np.sum(orb_indexed_array,axis=(2)),(self.n_bands,self.n_k)) 
                
                    
            else:
                proj_array[i,::,::] = np.reshape(orb_indexed_array,(self.n_bands,self.n_k) )
        return proj_array
    
   
    
    def DOS_full_new(self):
    
        E_min = np.nanmin(self.Es) - 5*self.sigma_smear
        E_max = np.nanmax(self.Es) + 5*self.sigma_smear
        # print("min/max: ",E_min,E_max)

        # print("starting broadcast")
        E_domain = np.broadcast_to(np.reshape(np.linspace(E_min,E_max, self.nE),(self.nE,1,1)),(self.nE,np.shape(self.Es)[0],np.shape(self.Es)[1]))
        # print(type(E_domain),np.shape(E_domain),E_domain.size,E_domain.itemsize)
        # print("finished broadcast, array size (MB): ~", print((E_domain.size*E_domain.itemsize)/1e6))
        
        dE = E_domain[1,0,0] - E_domain[0,0,0]
        
        
        gaussians = self.weights * self.mult_array * gauss(E_domain,self.Es,self.sigma_smear)
    
        
        integral = np.sum(gaussians,axis=(1,2))

                    
        Efmask = np.where(E_domain[::,0,0]<0,True,False)
        idos = (np.cumsum(integral))*dE
        sumval_dos = ((idos)[Efmask])[-1]
    
        
        factor = 1/sumval_dos
        return E_domain[::,0,0], (factor*integral),idos*factor, dE, factor
    
    def plot(self):
            
        print("calculation complete")
        if self.show:
            fig,ax = plt.subplots()
        
        
        plt.title("monolayer changing orbital project")
        
        plt.plot( self.E_dosplot,self.dosplot,marker=".",label=self.plotlabel)
        
        
        plt.xlim(-0.4,0.4)
        plt.ylim(0,1.1*np.nanmax(self.dosplot))
        
        plt.legend()
        
        plt.xlabel("E (eV)")
        
        # for ax in fig.get_axes():
        #     ax.label_outer()
        
        if self.show:
            plt.show()

        
        
    
        
        return True
    
    

